import React from "react";

function Navbar() {
    return (
        <nav>
            <a href="#dashboard" id="dashboard">Dashboard</a>
            <a href="#students" id="students" className="active">Students</a>
            <a href="#tasks" id="tasks">Tasks</a>
        </nav>
    )
}

export default Navbar;