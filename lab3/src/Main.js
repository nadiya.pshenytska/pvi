import React, { useState } from "react";
import ModalAdd from './Modal-Add.js';
import addImg from "./images/add.svg";
import TableRow from "./TableRow.js";


function Main() {

    const [showModal, setShowModal] = useState(false);
    const [tableData, setTableData] = useState([
        { id: 1, group: 'PZ-25', name: 'Nadia Pshenytska', gender: 'F', birthday: '06.10.2004', status: false },
        { id: 2, group: 'PZ-15', name: 'Adam Jensen', gender: 'M', birthday: '13.11.2004', status: false },
        { id: 3, group: 'PZ-21', name: 'Nancy Lockwood', gender: 'F', birthday: '27.04.2003', status: true },
    ])

    function addTableRow(newPerson) {
        const newRow = { id: tableData.length + 1, group: newPerson.group, name: newPerson.name, gender: newPerson.gender, birthday: newPerson.birthday, status: newPerson.status };
        setTableData([...tableData, newRow]);
    }

    function editTableRow(newData) {
        setTableData(tableData.map((row) => {
            if (row.id === newData.id) {
                return newData;
            }
            return row;
        }))
    }

    function removeTableRow(id) {
        setTableData(tableData.filter((row) => row.id !== id));
    }

    return (
        <main>
            <h2>Students</h2>
            <button className="addBtn" onClick={() => { setShowModal(true) }}><img src={addImg} alt="add" /></button>
            <table>
                <thead>
                    <tr><td className="col-checkbox"></td>
                        <td className="col-group">Group</td>
                        <td className="col-name">Name</td>
                        <td className="col-gender">Gender</td>
                        <td className="col-birthday">Birthday</td>
                        <td className="col-status">Status</td>
                        <td className="col-options">Options</td></tr>
                </thead>
                <tbody>
                    {
                        tableData.map((person) => (
                            <TableRow key={person.id} person={person} removeTableRow={removeTableRow} editTableRow={editTableRow} />
                        ))
                    }
                </tbody>
            </table>
            {showModal && <ModalAdd showModal={showModal} setModal={setShowModal} addTableRow={addTableRow} />}
        </main>
    )
}

export default Main;

/*
                    <tr>
                        <td className="col-checkbox">
                            <input type="checkbox" name="" id="" />
                        </td>
                        <td className="col-group">PZ-15</td>
                        <td className="col-name">Adam Jensen</td>
                        <td className="col-gender">M</td>
                        <td className="col-birthday">13.11.1987</td>
                        <td className="col-status">
                            <div className="status"></div>
                        </td>
                        <td className="col-options">
                            <div className="options">
                                <button className="editBtn">
                                    <img src={editImg} alt="" /></button><button className="deleteBtn">
                                    <img src={deleteImg} alt="" />
                                </button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td className="col-checkbox">
                            <input type="checkbox" name="" id="" />
                        </td>
                        <td className="col-group">PZ-26</td>
                        <td className="col-name">Nancy Lockwood</td>
                        <td className="col-gender">F</td>
                        <td className="col-birthday">27.04.2002</td>
                        <td className="col-status">
                            <div className="status status-active"></div>
                        </td>
                        <td className="col-options">
                            <div className="options">
                                <button className="editBtn">
                                    <img src={editImg} alt="" /></button><button className="deleteBtn">
                                    <img src={deleteImg} alt="" />
                                </button>
                            </div>
                        </td>
                    </tr> */