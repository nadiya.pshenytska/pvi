import React, { useState } from "react";
import editImg from "./images/edit.svg";
import deleteImg from "./images/delete.svg";
import ModalEdit from "./Modal-Edit";
import ModalWarning from "./Modal-Warning";

function TableRow({ person, removeTableRow, editTableRow }) {

    const [showModal, setShowModal] = useState(false);
    const [showWarnModal, setShowWarnModal] = useState(false);
    const { group, name, gender, birthday, status } = person
    return (<tr>
        <td className="col-checkbox">
            <input type="checkbox" name="" id="" />
            {
                showModal && <ModalEdit showModal={showModal} setModal={setShowModal} person={person} editTableRow={editTableRow} />}
            {
                showWarnModal && <ModalWarning showWarnModal={showWarnModal} setWarnModal={setShowWarnModal} person={person} removeTableRow={removeTableRow} />
            }
        </td>
        <td className="col-group">{group}</td>
        <td className="col-name"><span>{name}</span></td>
        <td className="col-gender">{gender}</td>
        <td className="col-birthday">{birthday}</td>
        <td className="col-status">
            <div className={status ? "status status-active" : "status"}></div>
        </td>
        <td className="col-options">
            <div className="options">
                <button className="editBtn">
                    <img src={editImg} alt="edit" onClick={() => { setShowModal(true) }} /></button><button className="deleteBtn">
                    <img src={deleteImg} alt="remove" onClick={() => { setShowWarnModal(true) }} />
                </button>
            </div>
        </td>
    </tr>)
}

export default TableRow;