/* eslint-disable no-restricted-globals */

const CACHE_NAME = 'my-cache';
const assets = ['/index.html', '/manifest.json', '/favicon.ico']

self.addEventListener('install', event => {
    event.waitUntil(
        caches.open(CACHE_NAME).then(cache => {
            cache.addAll(assets);
        })
    );
});

self.addEventListener('fetch', event => {
    event.respondWith(
        caches.match(event.request).then(response => {
            return response || fetch(event.request);
        })
    );
});