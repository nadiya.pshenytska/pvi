<?php

header("Access-Control-Allow-Origin: http://localhost:3000");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Allow-Headers: Content-Type");

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $message = "success";
    $firstName = $lastName = $birthday = $gender = $group = "";
    $firstName_err = $lastName_err = $birthday_err = $gender_err = $group_err = "";
    $data = json_decode(file_get_contents("php://input"), true);

    $firstName = trim($data['firstName']);
    $lastName = trim($data["lastName"]);
    $birthday = trim($data["birthday"]);
    $gender = trim($data["gender"]);
    $group = trim($data["group"]);

    if (empty($firstName)) {
        $firstName_err = "Please enter first name.";
        $message = "error";
    }

    if (empty($lastName)) {
        $lastName_err = "Please enter last name.";
        $message = "error";
    }

    if (empty($birthday)) {
        $birthday_err = "Please enter the date of birth.";
        $message = "error";
    }

    if (empty($gender)) {
        $gender_err = "Please enter your gender.";
        $message = "error";
    }

    if (empty($group)) {
        $group_err = "Please enter your group.";
        $message = "error";
    }

    $response = array(
        "message" => $message,
        "errors" => array($firstName_err, $lastName_err, $birthday_err, $gender_err, $group_err)
    );

    header('Content-Type: application/json');
    echo json_encode($response);
}
?>