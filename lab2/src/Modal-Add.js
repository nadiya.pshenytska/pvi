import React, { useState, useRef } from "react";
import closeImg from "./images/delete.svg";

function ModalAdd({ showModal, setModal, addTableRow }) {
    const [isClosed, setIsClosed] = useState(!showModal)
    const formRef = useRef(null)

    const closeModal = () => {
        setIsClosed(true)
        setModal(!showModal)
    }

    const submitForm = () => {
        const myForm = formRef.current

        if (myForm.checkValidity()) {
            const newPerson = {
                name: myForm[0].value + ' ' + myForm[1].value,
                group: myForm[4].value,
                gender: myForm[3].value[0].toUpperCase(),
                birthday: myForm[2].value.split('-').reverse().join('.'),
                status: true,
            }

            setIsClosed(true)
            setModal(!showModal)
            addTableRow(newPerson)
        }
    }

    return (
        <div className={isClosed ? "modal-content add-modal" : "modal-content add-modal modal-content-active"}>
            <div className="modal-header">
                <h2>Add student</h2>
                <button className="closeBtn" onClick={closeModal}>
                    <img src={closeImg} alt="close" />
                </button>
            </div>
            <div className="modal-body">
                <form id="input-form" ref={formRef}>
                    <div className="form-group">
                        <label htmlFor="first-name-input">First name: </label>
                        <input type="text" id="first-name-input" required />
                    </div>
                    <div className="form-group">
                        <label htmlFor="last-name-input">Last name: </label>
                        <input type="text" id="last-name-input" required />
                    </div>
                    <div className="form-group">
                        <label htmlFor="birthday-input">Birthday: </label>
                        <input type="date" id="birthday-input" max={new Date().toISOString().split("T")[0]} required />
                    </div>
                    <div className="form-group">
                        <label htmlFor="gender-input">Gender: </label>
                        <select name="gender-input" id="gender-input" required>
                            <option value="">Select gender</option>
                            <option value="male">Male</option>
                            <option value="female">Female</option>
                            <option value="other">Other</option>
                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor="group-input">Group</label>
                        <select name="group-input" id="group-input" required>
                            <option value="">Select group</option>
                            <option value="PZ-11">PZ-11</option>
                            <option value="PZ-12">PZ-12</option>
                            <option value="PZ-13">PZ-13</option>
                            <option value="PZ-14">PZ-14</option>
                            <option value="PZ-15">PZ-15</option>
                            <option value="PZ-16">PZ-16</option>
                            <option value="PZ-17">PZ-17</option>
                            <option value="PZ-18">PZ-18</option>
                            <option value="PZ-21">PZ-21</option>
                            <option value="PZ-22">PZ-22</option>
                            <option value="PZ-23">PZ-23</option>
                            <option value="PZ-24">PZ-24</option>
                            <option value="PZ-25">PZ-25</option>
                            <option value="PZ-26">PZ-26</option>
                        </select>
                    </div>
                </form>
            </div>
            <div className="modal-footer">
                <button type="submit" className="submitBtn" onClick={submitForm}>Save</button>
            </div>
        </div>
    )
}

export default ModalAdd;