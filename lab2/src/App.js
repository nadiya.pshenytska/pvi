
import './App.css';
import Header from './Header.js';
import Footer from './Footer.js';
import Main from './Main.js';
import Navbar from './Navbar.js';

function App() {
  // const { isLoading, error, data } = useQuery('myData', () =>
  //   fetch('https://my-api.com/data').then((res) => res.json())
  // );

  // if (isLoading) return <p>Loading...</p>;
  // if (error) return <p>Error: {error.message}</p>;

  // console.log(data)

  return (
    <div className="App">
      <Header />
      <Navbar />
      <Main />
      <Footer />
    </div>
  );
}

export default App;
