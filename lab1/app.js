const today = new Date();
const formattedToday = today.toISOString().split('T')[0];
document.getElementById('birthday-input').setAttribute('max', formattedToday);

const addButton = document.getElementsByClassName('addBtn')[0]
const submitBtn = document.getElementsByClassName('submitBtn')[0]

const notification = document.getElementsByClassName('notification')[0]
const addStudentModalWindow = document.getElementsByClassName('add-modal')[0]

console.log(notification.getElementsByClassName('new-notification')[0])

notification.addEventListener('dblclick', () => {
    if (notification.getElementsByClassName('new-notification')[0].classList.contains("notification-active")) {
        notification.getElementsByClassName('new-notification')[0].classList.remove("notification-active")
        console.log(notification.getElementsByClassName('new-notification')[0].classList);
        console.log('contains');
    }
    else {
        notification.getElementsByClassName('new-notification')[0].classList.add("notification-active")
        console.log('not contains');
    }
    // if (notification.getElementsByClassName('new-notification')[0].style.display == 'inline-block') {
    //     notification.getElementsByClassName('new-notification')[0].style.display = 'none'
    // }
    // else {
    //     notification.getElementsByClassName('new-notification')[0].style.display = 'inline-block'
    // }
})

const deleteStudentModalWindon = document.getElementsByClassName('warning-modal')[0]

let studentsAdded = 0

addButton.onclick = () => {
    addStudentModalWindow.classList.add('modal-content-active')
    console.log('function executed!');
}

const closeModal = () => {
    addStudentModalWindow.classList.remove('modal-content-active')
    deleteStudentModalWindon.classList.remove('modal-content-active')
    deleteStudentModalWindon.getElementsByTagName('h4')[0].innerHTML = 'Are you sure you want to delete user'
    console.log('function2 executed!');
}

submitBtn.onclick = () => {
    const form = document.getElementById('input-form')
    if (form.checkValidity()) {
        console.log('valid')
        const firstName = document.getElementById('first-name-input').value
        const lastName = document.getElementById('last-name-input').value
        const birthday = new Date(document.getElementById('birthday-input').value).toLocaleDateString('en-GB', {
            day: '2-digit',
            month: '2-digit',
            year: 'numeric'
        }).split('/').join('.')
        const gender = document.getElementById('gender-input').value
        const group = document.getElementById('group-input').value
        const person = {
            firstName, lastName, birthday, gender, group
        }
        console.log(person);
        addStudent(person)
    }
}

const addStudent = (student) => {
    const form = document.getElementById('input-form')
    const table = document.getElementsByTagName('table')[0]

    let checkbox = document.createElement('input')
    checkbox.type = 'checkbox'

    let group = document.createElement('span')
    group.innerHTML = student.group

    let name = document.createElement('span')
    name.innerHTML = `${student.firstName} ${student.lastName}`

    let birthday = document.createElement('span')
    birthday.innerHTML = student.birthday

    let status = document.createElement('div')

    studentsAdded % 2 == 0 ?
        status.classList = 'status status-active' :
        status.classList = 'status'

    let gender = document.createElement('span')
    gender.innerHTML = student.gender[0].toUpperCase()

    let editImg = document.createElement('img')
    editImg.src = 'images/edit.svg'

    let deleteImg = document.createElement('img')
    deleteImg.src = 'images/delete.svg'

    let editBtn = document.createElement('button')
    editBtn.appendChild(editImg)
    editBtn.className = 'editBtn'

    let deleteBtn = document.createElement('button')
    deleteBtn.appendChild(deleteImg)
    deleteBtn.className = 'deleteBtn'
    deleteBtn.onclick = () => {
        deleteStudent(deleteBtn)
    }

    let options = document.createElement('div')
    options.className = 'options'
    options.append(editBtn, deleteBtn)

    let row = table.insertRow()
    row.insertCell().appendChild(checkbox)
    row.lastChild.className = "col-checkbox"
    row.insertCell().appendChild(group)
    row.lastChild.className = "col-group"
    row.insertCell().appendChild(name)
    row.lastChild.className = "col-name"
    row.insertCell().appendChild(gender)
    row.lastChild.className = "col-gender"
    row.insertCell().appendChild(birthday)
    row.lastChild.className = "col-birthday"
    row.insertCell().appendChild(status)
    row.lastChild.className = "col-status"
    row.insertCell().appendChild(options)
    row.lastChild.className = "col-options"

    console.log(student)
    console.log(row)
    closeModal()
    form.reset()
    console.log(`Added: ${++studentsAdded}`)
}

const deleteStudent = (button) => {
    const row = button.closest('tr')

    const approveBtn = document.getElementsByClassName('warn-deleteBtn')[0]
    deleteStudentModalWindon.getElementsByTagName('h4')[0].innerHTML += ` ${row.getElementsByClassName('col-name')[0].innerHTML} ?`

    approveBtn.onclick = () => {
        row.remove()
        closeModal()
    }

    deleteStudentModalWindon.classList.add('modal-content-active')
}