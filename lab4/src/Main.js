import React, { useState, useEffect } from "react";
import ModalAdd from './Modal-Add.js';
import addImg from "./images/add.svg";
import TableRow from "./TableRow.js";
const url = 'http://localhost:8000/server.php'

function Main() {

    const [showModal, setShowModal] = useState(false);
    const [tableData, setTableData] = useState([])

    const fetchStudents = async () => {
        try {
            const response = await fetch(url)
            const students = await response.json()
            console.log(students)
            setTableData(students.map(student => {
                return {
                    id: student.id,
                    name: student.firstName + ' ' + student.lastName,
                    birthday: student.birthday,
                    group: student.studyGroup,
                    gender: student.gender,
                    status: student.onlineStatus
                }
            }))
        }
        catch (err) {
            alert(err)
        }
    }

    useEffect(() => {
        fetchStudents()
    }, [])

    function addTableRow(newPerson) {
        const newRow = { id: tableData.length + 1, group: newPerson.group, name: newPerson.name, gender: newPerson.gender, birthday: newPerson.birthday, status: newPerson.status };
        setTableData([...tableData, newRow]);
    }


    function removeTableRow(id) {
        fetch(url, {
            method: 'DELETE',
            body: JSON.stringify({ id: id }),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        })
            .then(response => response.json())
            .then(data => {
                if (data.id === id) {
                    setTableData(tableData.filter((row) => row.id !== id));
                }
            })
            .catch(err => alert(err))
    }

    return (
        <main>
            <h2>Students</h2>
            <button className="addBtn" onClick={() => { setShowModal(true) }}><img src={addImg} alt="add" /></button>
            <table>
                <thead>
                    <tr><td className="col-checkbox"></td>
                        <td className="col-group">Group</td>
                        <td className="col-name">Name</td>
                        <td className="col-gender">Gender</td>
                        <td className="col-birthday">Birthday</td>
                        <td className="col-status">Status</td>
                        <td className="col-options">Options</td></tr>
                </thead>
                <tbody>
                    {
                        tableData.map((person) => (
                            <TableRow key={person.id} person={person} removeTableRow={removeTableRow} editTableRow={fetchStudents} />
                        ))
                    }
                </tbody>
            </table>
            {showModal && <ModalAdd showModal={showModal} setModal={setShowModal} addTableRow={addTableRow} />}
        </main>
    )
}

export default Main;