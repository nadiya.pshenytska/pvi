import React, { useState } from "react";
import closeImg from "./images/delete.svg";

function ModalWarning({ showWarnModal, setWarnModal, person, removeTableRow }) {
    const [isClosed, setIsClosed] = useState(!showWarnModal)

    const closeModal = () => {
        setIsClosed(true)
        setWarnModal(false)
    }
    return (
        <div className={isClosed ? "modal-content warning-modal" : "modal-content warning-modal modal-content-active"}>
            <div className="modal-header">
                <h2>Warning</h2>
                <button className="closeBtn" onClick={closeModal}>
                    <img src={closeImg} alt="close" />
                </button>
            </div>
            <div className="modal-body">
                <h4>Are you sure you want to delete {person.name}?</h4>
            </div>
            <div className="modal-footer">
                <button className="warn-deleteBtn" onClick={() => {
                    removeTableRow(person.id)
                    closeModal()
                }}>Ok</button>
            </div>
        </div>)
}

export default ModalWarning;