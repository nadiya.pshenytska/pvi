import React, { useState } from "react";
import userImg from "./images/user.svg";
import notificationImg from "./images/notification.svg";

function Header() {
    const [isDoubleClicked, setIsDoubleClicked] = useState(false)

    const notificationDblClick = () => {
        setIsDoubleClicked(!isDoubleClicked);
    }

    return (
        <header>
            <h2>CMS</h2>
            <div className="menu">
                <div className="notification" onDoubleClick={notificationDblClick}>
                    <img src={notificationImg} alt="" />
                    <div className={isDoubleClicked ? "new-notification notification-active" : "new-notification"} ></div>
                    <div className="dropdown-content">
                        <div className="dropdown-elem">
                            <div className="user-content">
                                <div className="user-img">
                                    <img src={userImg} alt="user" />
                                </div>
                                <div className="user-message"></div>
                            </div>
                            <div className="user-name">Admin</div>
                        </div>
                        <div className="dropdown-elem">
                            <div className="user-content">
                                <div className="user-img">
                                    <img src={userImg} alt="user" />
                                </div>
                                <div className="user-message"></div>
                            </div>
                            <div className="user-name">Pamela Geller</div>
                        </div>
                    </div>
                </div>
                <div className="user-info">
                    <div className="user-img"><img src={userImg} alt="" /></div>
                    <div className="user-name">
                        <p>Nadiia Pshenytska</p>
                    </div>
                    <div className="dropdown-content">
                        <div className="dropdown-elem">
                            <div className="profile">
                                <a href="#profile" id="profile">Profile</a>
                            </div>
                            <div className="logout">
                                <a href="#logout" id="logout">Log Out</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    )
}

export default Header;