<?php
class Student
{
    private $id;
    private $firstName;
    private $lastName;
    private $gender;
    private $group;
    private $birthday;

    public function __construct($id, $firstName, $lastName, $gender, $group, $birthday)
    {
        $this->id = $id;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->gender = $gender;
        $this->group = $group;
        $this->birthday = $birthday;
    }

    public function getId()
    {
        return $this->id;
    }
    public function getFirstName()
    {
        return $this->firstName;
    }
    public function getLastName()
    {
        return $this->lastName;
    }
    public function getGender()
    {
        return $this->gender;
    }
    public function getGroup()
    {
        return $this->group;
    }
    public function getBirthday()
    {
        return $this->birthday;
    }

    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }
    public function setGender($gender)
    {
        $this->gender = $gender;
    }
    public function setGroupId($group)
    {
        $this->group = $group;
    }
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
    }

}
?>