<?php

include 'student.php';

class StudentDAO
{
    private $pdo;

    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    function verify($data)
    {
        $message = "success";

        $firstName = $lastName = $birthday = $gender = $group = "";
        $firstName_err = $lastName_err = $birthday_err = $gender_err = $group_err = "";

        $firstName = trim($data['firstName']);
        $lastName = trim($data["lastName"]);
        $birthday = trim($data["birthday"]);
        $gender = trim($data["gender"]);
        $group = trim($data["group"]);

        if (empty($firstName)) {
            $firstName_err = "Please enter first name.";
            $message = "error";
        }

        if (empty($lastName)) {
            $lastName_err = "Please enter last name.";
            $message = "error";
        }

        if (empty($birthday)) {
            $birthday_err = "Please enter the date of birth.";
            $message = "error";
        }

        if (empty($gender)) {
            $gender_err = "Please enter your gender.";
            $message = "error";
        }

        if (empty($group)) {
            $group_err = "Please enter your group.";
            $message = "error";
        }

        $response = array(
            "message" => $message,
            "errors" => array($firstName_err, $lastName_err, $birthday_err, $gender_err, $group_err)
        );

        return $response;
    }

    private function getStudentGroupById($groupId)
    {
        $groupsList = $this->pdo->prepare("SELECT id FROM student_groups WHERE id = ?");
        $groupsList->execute([$groupId]);
        return $groupsList;
    }

    private function getStudentById($studentId)
    {
        $studentIds = $this->pdo->prepare("SELECT id FROM students WHERE id = ?");
        $studentIds->execute([$studentId]);
        return $studentIds;
    }

    public function getStudents()
    {
        $stmt = $this->pdo->prepare('SELECT * FROM students');
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function createStudent($student)
    {
        $verifiedStudent = $this->verify($student);

        $firstName = trim($student['firstName']);
        $lastName = trim($student["lastName"]);
        $birthday = trim($student["birthday"]);
        $gender = trim($student["gender"]);
        $group = trim($student["group"]);
        if ($verifiedStudent['message'] !== 'error') {
            $stmt = $this->pdo->prepare('INSERT INTO students (firstName, lastName,studyGroup,gender, birthday) VALUES (?, ?, ?, ?, ?)');
            $stmt->execute([
                $firstName,
                $lastName,
                $group,
                $gender,
                $birthday,
            ]);
        }
        return $verifiedStudent;
    }

    public function updateStudent($student)
    {
        $response = $this->verify($student);

        if ($response["message"] !== "error") {
            $firstName = trim($student['firstName']);
            $lastName = trim($student["lastName"]);
            $birthday = trim($student["birthday"]);
            $gender = trim($student["gender"]);
            $group = trim($student["group"]);
            $id = trim($student["id"]);
            $stmt = $this->pdo->prepare('UPDATE students SET firstName = ?, lastName = ?, studyGroup = ?, gender = ?, birthday = ? WHERE id = ?');
            $stmt->execute([
                $firstName,
                $lastName,
                $group,
                $gender,
                $birthday,
                $id
            ]);

        }
        return $response;
    }

    public function deleteStudentById($id)
    {
        $stmt = $this->pdo->prepare('DELETE FROM students WHERE id = ?');
        $success = $stmt->execute([$id]);
        return $success;
    }

    public function newStudentObj($data)
    {
        return new Student(
            $data['id'], $data['firstName'], $data['lastName'],
            $data['gender'], $data['group'], $data['birthday']
        );
    }
}
?>