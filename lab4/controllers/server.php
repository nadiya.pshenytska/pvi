<?php

include 'db.php';
include '../models/studentDAO.php';

header("Access-Control-Allow-Origin: http://localhost:3000");
header("Access-Control-Allow-Methods: *");
header("Access-Control-Allow-Headers: Content-Type");

$studentDAO = new StudentDAO($pdo);

if ($_SERVER["REQUEST_METHOD"] === "POST") {

    $data = json_decode(file_get_contents("php://input"), true);

    $response = $studentDAO->createStudent($data);

    if ($response["message"] == "error") {
        http_response_code(400);
        echo json_encode($response);
        exit();
    }

    header('Content-Type: application/json');
    echo json_encode($response);
}

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $users = $studentDAO->getStudents();
    header('Content-Type: application/json');
    echo json_encode($users);
}

if ($_SERVER['REQUEST_METHOD'] === 'PUT') {
    $data = json_decode(file_get_contents('php://input'), true);

    $result = $studentDAO->updateStudent($data);
    if ($result["message"] === "error") {
        http_response_code(400);
        echo json_encode($result);
        exit();
    }

    header('Content-Type: application/json');
    echo json_encode(['id' => $data['id'], 'data' => $data]);
}



if ($_SERVER['REQUEST_METHOD'] === 'DELETE') {
    $id = json_decode(file_get_contents("php://input"), true)['id'];
    $result = $studentDAO->deleteStudentById($id);

    if ($result) {
        header('Content-Type: application/json');
        echo json_encode(['id' => $id]);
        exit();
    }
    http_response_code(400);
    echo json_encode(['error' => "Error with deletion"]);

}

?>